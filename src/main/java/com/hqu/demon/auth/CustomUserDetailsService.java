package com.hqu.demon.auth;

import com.hqu.demon.dao.UserMapper;
import com.hqu.demon.entity.User;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * 用于加载用户信息 实现UserDetailsService接口，或者实现AuthenticationUserDetailsService接口
 *
 * @author ChengLi
 * //实现AuthenticationUserDetailsService，实现loadUserDetails方法
 */
public class CustomUserDetailsService implements AuthenticationUserDetailsService<CasAssertionAuthenticationToken> {

    private UserMapper userMapper;

    public CustomUserDetailsService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

/*
	//实现UserDetailsService接口，实现loadUserByUsername方法
	implements UserDetailsService {
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("当前的用户名是："+username);
		//这里我为了方便，就直接返回一个用户信息，实际当中这里修改为查询数据库或者调用服务什么的来获取用户信息
		UserInfo userInfo = new UserInfo();
		userInfo.setUsername("admin");
		userInfo.setName("admin");
		Set<AuthorityInfo> authorities = new HashSet<AuthorityInfo>();
		AuthorityInfo authorityInfo = new AuthorityInfo("TEST");
		authorities.add(authorityInfo);
		userInfo.setAuthorities(authorities);
		return userInfo;
	}*/


    @Override
    public UserDetails loadUserDetails(CasAssertionAuthenticationToken token) throws UsernameNotFoundException {
        System.out.println("当前的用户名是：" + token.getName());
        /*这里我为了方便，就直接返回一个用户信息，实际当中这里修改为查询数据库或者调用服务什么的来获取用户信息*/
        User user = userMapper.selectByUsername(token.getName());
        if (user == null) {
            User newUser = new User();
            newUser.setUsername(token.getName());
            userMapper.insert(newUser);
            user = userMapper.selectByUsername(token.getName());
        }
//        Set<AuthorityInfo> authorities = new HashSet<AuthorityInfo>();
//        AuthorityInfo authorityInfo = new AuthorityInfo("TEST");
//        authorities.add(authorityInfo);
//        user.setAuthorities(authorities);
        return user;
    }

}