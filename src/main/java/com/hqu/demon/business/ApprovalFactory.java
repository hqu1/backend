package com.hqu.demon.business;

import com.google.common.primitives.Longs;
import com.hqu.demon.entity.Application;
import com.hqu.demon.entity.Approval;
import com.hqu.demon.enums.ApplicationTypeEnum;
import com.hqu.demon.enums.ApprovalStatusEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class ApprovalFactory {

    @Value("${app.approval-user.valuable}")
    private long[] valuableUserIds;
    @Value("${app.approval-user.import}")
    private long[] importUserIds;
    @Value("${app.approval-user.check}")
    private long[] checkUserIds;

    private List<Long> getApprovalUserIdsByApplicationType(ApplicationTypeEnum type) {
        switch (type) {
            case IMPORT_DEVICE:
                return Longs.asList(importUserIds);
            case CHECK_DEVICE:
                return Longs.asList(checkUserIds);
            case VALUABLE_DEVICE:
                return Longs.asList(valuableUserIds);
        }
        return Collections.emptyList();
    }

    /**
     * 创建审批列表
     *
     * @param application
     * @return
     */
    public List<Approval> createApplicationApprovals(Application application) {
        List<Long> userIds = getApprovalUserIdsByApplicationType(application.getType());
        AtomicInteger index = new AtomicInteger(0);
        return userIds.stream().map(userId -> {
            int i = index.getAndIncrement();
            Approval approval = createApproval(application, i, userId);
            return approval;
        }).collect(Collectors.toList());
    }

    private Approval createApproval(Application application, int sequence, Long userId) {
        Approval approval = new Approval();
        approval.setApplicationId(application.getApplicationId());
        approval.setSequence(sequence);
        ApprovalStatusEnum statusEnum = sequence == 0 ? ApprovalStatusEnum.PROCESSING : ApprovalStatusEnum.WAITING;
//        approval.setStatus(statusEnum.getValue());
        approval.setUserId(userId);
        return approval;
    }

}
