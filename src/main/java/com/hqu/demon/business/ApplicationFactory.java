package com.hqu.demon.business;

import com.hqu.demon.entity.Application;
import com.hqu.demon.entity.CheckDeviceApplication;
import com.hqu.demon.entity.ImportDeviceApplication;
import com.hqu.demon.entity.ValuableDeviceApplication;
import com.hqu.demon.enums.ApplicationTypeEnum;
import com.hqu.demon.pojo.request.BaseApplicationRequest;
import com.hqu.demon.pojo.request.CheckDeviceApplicationRequest;
import com.hqu.demon.pojo.request.ImportDeviceApplicationRequest;
import com.hqu.demon.pojo.request.ValuableDeviceApplicationRequest;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ApplicationFactory {
    public Application createValuableDeviceApplication(ValuableDeviceApplicationRequest request, Long userId) {
        ValuableDeviceApplication valuableDeviceApplication = new ValuableDeviceApplication();
        initBaseApplicationInfo(request, valuableDeviceApplication, userId);
        return valuableDeviceApplication;
    }

    public Application createImportDeviceApplication(ImportDeviceApplicationRequest request, Long userId) {
        ImportDeviceApplication importDeviceApplication = new ImportDeviceApplication();
        initBaseApplicationInfo(request, importDeviceApplication, userId);
        return importDeviceApplication;
    }

    public Application createCheckDeviceApplication(CheckDeviceApplicationRequest request, Long userId) {
        CheckDeviceApplication valuableDeviceApplication = new CheckDeviceApplication();
        initBaseApplicationInfo(request, valuableDeviceApplication, userId);
        return valuableDeviceApplication;
    }

    private Application initBaseApplicationInfo(BaseApplicationRequest baseApplicationRequest, Application application, Long userId) {
        application.setApplierId(userId);
        application.setApplierOrganId(baseApplicationRequest.getApplierOrganId());
        application.setInfo(baseApplicationRequest.getInfo());
        application.setApplyTime(new Date());
        application.setType(ApplicationTypeEnum.IMPORT_DEVICE);
        application.setStatus(baseApplicationRequest.getApplicationStatus());
        return application;
    }

}
