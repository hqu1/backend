package com.hqu.demon.business.utils;

import com.hqu.demon.enums.ApplicationStatusEnum;
import com.hqu.demon.enums.ApprovalStatusEnum;

public class EnumUtils {

    public static ApplicationStatusEnum convertFromApprovalStatus(ApprovalStatusEnum approvalStatusEnum) {
        switch (approvalStatusEnum) {
            case APPROVED:
                return ApplicationStatusEnum.APPROVED;
            case DECLINE:
                return ApplicationStatusEnum.DECLINE;
            default:
                return ApplicationStatusEnum.APPROVING;
        }
    }
}
