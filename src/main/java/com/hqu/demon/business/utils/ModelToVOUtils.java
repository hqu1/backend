package com.hqu.demon.business.utils;

import com.hqu.demon.entity.Resource;
import com.hqu.demon.pojo.response.ResourceVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;

@Component
public class ModelToVOUtils {

    private static final Logger logger = LoggerFactory.getLogger(ModelToVOUtils.class);

    public ResourceVO convertResourceToVO(Resource ossResource) {
        ResourceVO vo = new ResourceVO();
        BeanUtils.copyProperties(vo, ossResource);
        vo.setUrl(contactFileUrl(ossResource.getPath() + File.separator + ossResource.getFileName()));
        return vo;
    }

    public String contactFileUrl(String url) {
        if (StringUtils.isEmpty(url)) {
            return "";
        }
        if (isURL(url)) {
            return url;
        }
        // TODO inject file server
//        return feiyaFileServer + url;
        return url;
    }

    private static boolean isURL(String str) {
        str = str.toLowerCase();
        String regex = "^((https|http|ftp|rtsp|mms)?://)" //https、http、ftp、rtsp、mms
                + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@  
                + "(([0-9]{1,3}\\.){3}[0-9]{1,3}" // IP形式的URL- 例如：199.194.52.184  
                + "|" // 允许IP和DOMAIN（域名）
                + "([0-9a-z_!~*'()-]+\\.)*" // 域名- www.  
                + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\\." // 二级域名  
                + "[a-z]{2,6})" // first level domain- .com or .museum  
                + "(:[0-9]{1,5})?" // 端口号最大为65535,5位数
                + "((/?)|" // a slash isn't required if there is no file name  
                + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
        return str.matches(regex);
    }
}
