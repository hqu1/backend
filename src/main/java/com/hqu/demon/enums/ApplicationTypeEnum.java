package com.hqu.demon.enums;

import java.security.InvalidParameterException;

public enum ApplicationTypeEnum implements BaseEnum<ApplicationTypeEnum> {

    VALUABLE_DEVICE(1, "贵仪设备可行性验证申请"),
    CHECK_DEVICE(2, "设备验收申请"),
    IMPORT_DEVICE(3, "进口设备申请");


    private int value;
    private String displayName;

    ApplicationTypeEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int getValue() {
        return value;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public static ApplicationTypeEnum fromValue(int value) {
        for (ApplicationTypeEnum typeEnum : ApplicationTypeEnum.values()) {
            if (typeEnum.getValue() == value) {
                return typeEnum;
            }
        }
        throw new InvalidParameterException("invalid application type enum value:" + value);
    }
}
