package com.hqu.demon.enums;

public interface BaseEnum<E extends Enum<?>> {

    public int getValue();

}
