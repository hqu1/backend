package com.hqu.demon.enums;

public enum ApplicationStatusEnum implements BaseEnum<ApplicationStatusEnum> {
    DRAFT(1, ""),
    APPROVING(2, ""),
    APPROVED(3, ""),
    DECLINE(4, "");

    private int value;
    private String displayName;

    ApplicationStatusEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int getValue() {
        return value;
    }

    public String getDisplayName() {
        return displayName;
    }

}
