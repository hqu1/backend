package com.hqu.demon.enums;

public enum ErrorCodeEnum {

    APPLICATION_NOT_EXIST(1001, "申请单不存在"),
    APPLICATION_INVALID_STATUS(1002, "申请单状态异常，无法进行审批"),

    APPROVAL_NOT_EXIST(1101, "审批不存在"),
    APPROVAL_INVALID_STATUS(1002, "审批状态异常，无法进行审批"),

    RESOURCE_NOT_EXIST(1201, "附件不存在"),

    AUTH_FAILURE(1301, "认证失败");

    private ErrorCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    private Integer code;
    private String message;


    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
