package com.hqu.demon.enums;

public enum ApprovalStatusEnum implements BaseEnum<ApprovalStatusEnum> {

    WAITING(1, ""),
    PROCESSING(2, ""),
    APPROVED(3, ""),
    DECLINE(4, "");


    private int value;
    private String displayName;

    ApprovalStatusEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
