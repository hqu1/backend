package com.hqu.demon.controller;

import com.hqu.demon.entity.Application;
import com.hqu.demon.pojo.request.CheckDeviceApplicationRequest;
import com.hqu.demon.pojo.request.ImportDeviceApplicationRequest;
import com.hqu.demon.pojo.request.QueryApplicationRequest;
import com.hqu.demon.pojo.request.ValuableDeviceApplicationRequest;
import com.hqu.demon.pojo.response.CommonResult;
import com.hqu.demon.service.ApplicationService;
import com.hqu.demon.auth.AuthUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Api("申请控制器")
@RestController
@RequestMapping("/applications")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    @ApiOperation("hello")
    @GetMapping("/hello")
    public String hello() {
        return "hello world!";
    }

    @ApiOperation("通过ID获取审批")
    @GetMapping("/{applicationId}")
    public CommonResult<Application> getApplication(@PathVariable("applicationId") @NotNull Long applicationId) {
        return new CommonResult<>(applicationService.getApplicationDetail(applicationId));
    }

    @ApiOperation("搜索申请列表")
    @GetMapping("/")
    public CommonResult<List<Application>> getApplicationList(@ModelAttribute QueryApplicationRequest request) {
        long userId = AuthUtils.getLoginUser().getUserId();
        return new CommonResult<List<Application>>(applicationService.findApplicationList(request, userId));
    }

    @ApiOperation("创建贵仪设备审批")
    @PostMapping("/valuable-device")
    public CommonResult<Application> createValuableDeviceApplication(@RequestBody ValuableDeviceApplicationRequest request) {
        long userId = AuthUtils.getLoginUser().getUserId();
        return new CommonResult<>(applicationService.createValuableDeviceApplication(request, userId));
    }

    @ApiOperation("创建进口设备审批")
    @PostMapping("/import-device")
    public CommonResult<Application> createImportDeviceApplication(@RequestBody ImportDeviceApplicationRequest request) {
        long userId = AuthUtils.getLoginUser().getUserId();
        return new CommonResult<>(applicationService.createImportDeviceApplication(request, userId));
    }

    @ApiOperation("创建验收设备审批")
    @PostMapping("/check-device")
    public CommonResult<Application> createCheckDeviceApplication(@RequestBody CheckDeviceApplicationRequest request) {
        long userId = AuthUtils.getLoginUser().getUserId();
        return new CommonResult<>(applicationService.createCheckDeviceApplication(request, userId));
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
