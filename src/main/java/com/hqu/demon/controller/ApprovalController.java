package com.hqu.demon.controller;

import com.hqu.demon.entity.Approval;
import com.hqu.demon.pojo.request.ApproveApplicationRequest;
import com.hqu.demon.pojo.response.CommonResult;
import com.hqu.demon.service.ApprovalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/approvals")
@Api("审批控制器")
public class ApprovalController {

    @Autowired
    private ApprovalService approvalService;

    @PostMapping("/{approvalId}/approve")
    @ApiOperation("审批申请单")
    public CommonResult<Boolean> approve(@PathVariable Long approvalId, @RequestBody ApproveApplicationRequest request) {
        approvalService.approve(approvalId, 1L, request.getStatus(), request.getAdvice());
        return new CommonResult<>(true);
    }

    @PostMapping("/{approvalId}")
    @ApiOperation("Test")
    public CommonResult<Approval> getApproval(@PathVariable Long approvalId) {
        return new CommonResult<>(approvalService.getBaseMapper().selectByPrimaryKey(approvalId));
    }
}
