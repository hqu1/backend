package com.hqu.demon.controller;

import com.hqu.demon.business.utils.ModelToVOUtils;
import com.hqu.demon.entity.Resource;
import com.hqu.demon.enums.ErrorCodeEnum;
import com.hqu.demon.exception.CannotFindEntityException;
import com.hqu.demon.pojo.request.MultipartUploadResourceRequest;
import com.hqu.demon.pojo.response.CommonResult;
import com.hqu.demon.pojo.response.ResourceVO;
import com.hqu.demon.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Api(value = "资源服务", tags = "资源服务")
@RestController
@RequestMapping("/resources")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ModelToVOUtils modelToVOUtils;

    @Value("${resource.upload.path}")
    private String uploadPath;

    @PostMapping("/multipart")
    @ApiOperation(value = "上传Multipart类型文件")
    public CommonResult<List<ResourceVO>> uploadMultipartFile(@Valid @ModelAttribute MultipartUploadResourceRequest fileMultipartUploadRequestModel) {
        long currentUserId = 1L;
        MultipartFile[] resourceFiles = fileMultipartUploadRequestModel.getFiles();
        String path = uploadPath + fileMultipartUploadRequestModel.getFilePath() + File.separator;
        List<Resource> resources = resourceService.uploadMultipartFiles(resourceFiles, path, currentUserId);
        List<ResourceVO> resourceVOs = resources.stream().map(modelToVOUtils::convertResourceToVO).collect(Collectors.toList());
        return new CommonResult(resourceVOs);
    }

    @PostMapping("/{resourceId}")
    public CommonResult<ResourceVO> getResource(@PathVariable("resourceId") Long resourceId) {
        Resource resource = resourceService.getResourceById(resourceId);
        if (resource == null) {
            throw new CannotFindEntityException(ErrorCodeEnum.RESOURCE_NOT_EXIST);
        }
        return new CommonResult(modelToVOUtils.convertResourceToVO(resource));
    }
}
