package com.hqu.demon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hqu.demon.entity.Resource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ResourceMapper extends BaseMapper<Resource> {

    /**
     * 通过 MD5 查找资源
     *
     * @param fingerPrint
     * @return
     */
    Resource findByFingerPrint(@Param("fingerPrint") String fingerPrint);

}
