package com.hqu.demon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hqu.demon.entity.Application;
import com.hqu.demon.enums.ApplicationTypeEnum;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface ApplicationMapper extends BaseMapper<Application> {
    int deleteByPrimaryKey(Long applicationId);

    int insertSelective(Application record);

    Application selectByPrimaryKey(Long applicationId);

    int updateByPrimaryKeySelective(Application record);

    int updateByPrimaryKeyWithBLOBs(Application record);

    int updateByPrimaryKey(Application record);

    Application getApplicationDetail(@Param("applicationId") Long applicationId);

    IPage<Application> queryApplicationList(Page page,
                                            @Param("applicationType") ApplicationTypeEnum applicationType,
                                            @Param("deviceName") String deviceName,
                                            @Param("startDate") Date startDate,
                                            @Param("endDate") Date endDate,
                                            @Param("userId") Long userId);
}