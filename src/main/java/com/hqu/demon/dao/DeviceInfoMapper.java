package com.hqu.demon.dao;

import com.hqu.demon.entity.DeviceInfo;

public interface DeviceInfoMapper {
    int deleteByPrimaryKey(Long deviceInfoId);

    int insert(DeviceInfo record);

    int insertSelective(DeviceInfo record);

    DeviceInfo selectByPrimaryKey(Long deviceInfoId);

    int updateByPrimaryKeySelective(DeviceInfo record);

    int updateByPrimaryKey(DeviceInfo record);
}