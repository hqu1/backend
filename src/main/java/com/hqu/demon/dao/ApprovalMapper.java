package com.hqu.demon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hqu.demon.entity.Approval;

public interface ApprovalMapper extends BaseMapper<Approval> {
    int deleteByPrimaryKey(Long approvalId);

    int insertSelective(Approval record);

    Approval selectByPrimaryKey(Long approvalId);

    int updateByPrimaryKeySelective(Approval record);

    int updateByPrimaryKey(Approval record);
}