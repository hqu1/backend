package com.hqu.demon.exception;

import com.hqu.demon.enums.ErrorCodeEnum;

public class CannotFindEntityException extends BaseException {
    public CannotFindEntityException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum);
    }
}
