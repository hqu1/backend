package com.hqu.demon.exception;


import com.hqu.demon.enums.ErrorCodeEnum;

/**
 * 业务异常
 */
public class BusinessException extends BaseException {

    public BusinessException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum);
    }

}
