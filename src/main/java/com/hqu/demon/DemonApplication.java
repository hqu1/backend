package com.hqu.demon;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class DemonApplication {

    private static Logger logger = LoggerFactory.getLogger(DemonApplication.class);

    public static void main(String[] args) {
        logger.info("Spring Application Start!!!");
        SpringApplication.run(DemonApplication.class, args);
    }

}
