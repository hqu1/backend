package com.hqu.demon.utils;

import eu.medsea.mimeutil.MimeUtil;

import java.util.Collection;

public class FileUtils {

    public static String getFileMineType(byte[] data) {
        MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
        Collection<?> mimeTypes = MimeUtil.getMimeTypes(data);
        return mimeTypes.toString();
    }

    /*
     * Java文件操作 获取文件扩展名: .txt
     */
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot);
            }
        }
        return filename;
    }

}
