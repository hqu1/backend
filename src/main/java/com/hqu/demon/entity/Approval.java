package com.hqu.demon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hqu.demon.enums.ApplicationStatusEnum;
import com.hqu.demon.enums.ApprovalStatusEnum;

import java.util.Date;

public class Approval {
    @TableId(value = "approval_id", type = IdType.AUTO)
    private Long approvalId;

    private Long applicationId;

    private ApprovalStatusEnum status;

    private Long userId;

    private Integer sequence;

    private Date createdTime;

    private Date updatedTime;

    private String advice;

    public Approval(Long approvalId, Long applicationId, ApprovalStatusEnum status, Long userId, Integer sequence, Date createdTime, Date updatedTime, String advice) {
        this.approvalId = approvalId;
        this.applicationId = applicationId;
        this.status = status;
        this.userId = userId;
        this.sequence = sequence;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
        this.advice = advice;
    }

    public boolean canBeApproved() {
        return status == ApprovalStatusEnum.PROCESSING;
    }

    public Approval() {
        super();
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public ApprovalStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ApprovalStatusEnum status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice == null ? null : advice.trim();
    }
}