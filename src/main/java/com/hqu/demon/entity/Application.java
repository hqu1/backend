package com.hqu.demon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hqu.demon.enums.ApplicationStatusEnum;
import com.hqu.demon.enums.ApplicationTypeEnum;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Application implements Serializable {
    @TableId(value = "application_id", type = IdType.AUTO)
    private Long applicationId;

    private Long applierId;

    private Long applierOrganId;

    private Date createdTime;

    private Date updatedTime;

    private Date applyTime;

    private ApplicationStatusEnum status;

    private ApplicationTypeEnum type;

    private String info;

    private String deviceName;

    private List<Approval> approvalList;

    private User applier;

    public Application(Long applicationId, Long applierId, Long applierOrganId, Date createdTime, Date updatedTime, Date applyTime, ApplicationStatusEnum status, ApplicationTypeEnum type, String info) {
        this.applicationId = applicationId;
        this.applierId = applierId;
        this.applierOrganId = applierOrganId;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
        this.applyTime = applyTime;
        this.status = status;
        this.type = type;
        this.info = info;
    }

    public boolean canBeApproved() {
        return status == ApplicationStatusEnum.APPROVING;
    }

    public Application() {
        super();
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public Long getApplierId() {
        return applierId;
    }

    public void setApplierId(Long applierId) {
        this.applierId = applierId;
    }

    public Long getApplierOrganId() {
        return applierOrganId;
    }

    public void setApplierOrganId(Long applierOrganId) {
        this.applierOrganId = applierOrganId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public ApplicationStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ApplicationStatusEnum status) {
        this.status = status;
    }

    public ApplicationTypeEnum getType() {
        return type;
    }

    public void setType(ApplicationTypeEnum type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    public String getDeviceName() {
        return deviceName;
    }

    public Application setDeviceName(String deviceName) {
        this.deviceName = deviceName;
        return this;
    }

    public List<Approval> getApprovalList() {
        return approvalList;
    }


    public void setApprovalList(List<Approval> approvalList) {
        this.approvalList = approvalList;
    }

    public User getApplier() {
        return applier;
    }

    public Application setApplier(User applier) {
        this.applier = applier;
        return this;
    }
}