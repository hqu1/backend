package com.hqu.demon.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

public class Resource {

    @TableId(value = "resource_id", type = IdType.AUTO)
    private Long resourceId;

    /**
     * 资源名
     */
    private String name;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 路径
     */
    private String path;

    /**
     * 文件大小
     */
    private long size;

    /**
     * mine 类型
     */
    private String mimeType;

    /**
     * 文件内容唯一标记：md5
     */
    private String fingerPrint;

    /**
     * 更新时间
     */
    @TableField(updateStrategy = FieldStrategy.NEVER, insertStrategy = FieldStrategy.NEVER)
    private Date updatedTime;

    /**
     * 上传资源的用户 ID
     */
    private Long userId;

    public Long getResourceId() {
        return resourceId;
    }

    public Resource setResourceId(Long resourceId) {
        this.resourceId = resourceId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Resource setName(String name) {
        this.name = name;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public Resource setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getPath() {
        return path;
    }

    public Resource setPath(String path) {
        this.path = path;
        return this;
    }

    public long getSize() {
        return size;
    }

    public Resource setSize(long size) {
        this.size = size;
        return this;
    }

    public String getMimeType() {
        return mimeType;
    }

    public Resource setMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public String getFingerPrint() {
        return fingerPrint;
    }

    public Resource setFingerPrint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
        return this;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public Resource setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public Resource setUserId(Long userId) {
        this.userId = userId;
        return this;
    }
}
