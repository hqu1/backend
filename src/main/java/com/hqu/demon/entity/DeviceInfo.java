package com.hqu.demon.entity;

public class DeviceInfo {
    private Long deviceInfoId;

    private String chineseName;

    private String foreignName;

    private String deviceType;

    private String specification;

    private String factory;

    private String country;

    private Float price;

    private String brand;

    private Integer amount;

    public DeviceInfo(Long deviceInfoId, String chineseName, String foreignName, String deviceType, String specification, String factory, String country, Float price, String brand, Integer amount) {
        this.deviceInfoId = deviceInfoId;
        this.chineseName = chineseName;
        this.foreignName = foreignName;
        this.deviceType = deviceType;
        this.specification = specification;
        this.factory = factory;
        this.country = country;
        this.price = price;
        this.brand = brand;
        this.amount = amount;
    }

    public DeviceInfo() {
        super();
    }

    public Long getDeviceInfoId() {
        return deviceInfoId;
    }

    public void setDeviceInfoId(Long deviceInfoId) {
        this.deviceInfoId = deviceInfoId;
    }

    public String getChineseName() {
        return chineseName;
    }

    public void setChineseName(String chineseName) {
        this.chineseName = chineseName == null ? null : chineseName.trim();
    }

    public String getForeignName() {
        return foreignName;
    }

    public void setForeignName(String foreignName) {
        this.foreignName = foreignName == null ? null : foreignName.trim();
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType == null ? null : deviceType.trim();
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification == null ? null : specification.trim();
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory == null ? null : factory.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand == null ? null : brand.trim();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}