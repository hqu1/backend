package com.hqu.demon.pojo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 *
 */
@ApiModel("多文件上传请求参数")
public class MultipartUploadResourceRequest {
    @ApiModelProperty(value = "Multipart类型文件")
    @NotNull
    private MultipartFile[] files;
    @ApiModelProperty(value = "文件路径")
    private String filePath;

    public MultipartFile[] getFiles() {
        return files;
    }

    public MultipartUploadResourceRequest setFiles(MultipartFile[] files) {
        this.files = files;
        return this;
    }

    public String getFilePath() {
        return filePath;
    }

    public MultipartUploadResourceRequest setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }
}
