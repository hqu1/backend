package com.hqu.demon.pojo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel("分页参数")
public class PageRequest {

    @ApiModelProperty("当前页")
    @NotNull
    private int currentPage;

    @ApiModelProperty("每页大小")
    @NotNull
    private int pageSize;

    public int getCurrentPage() {
        return currentPage;
    }

    public PageRequest setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public PageRequest setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }
}
