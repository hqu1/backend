package com.hqu.demon.pojo.request;

import com.hqu.demon.enums.ApprovalStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel("审批申请单请求参数")
public class ApproveApplicationRequest {
    @ApiModelProperty("审批状态：APPROVED：审批通过，DECLINE：拒绝审批")
    @NotNull
    private ApprovalStatusEnum status;
    @ApiModelProperty("审批意见")
    private String advice;

    public ApprovalStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ApprovalStatusEnum status) {
        this.status = status;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }
}
