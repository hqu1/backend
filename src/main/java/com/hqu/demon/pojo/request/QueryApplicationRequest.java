package com.hqu.demon.pojo.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hqu.demon.enums.ApplicationTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel("查询申请列表参数")
public class QueryApplicationRequest extends PageRequest {

    @ApiModelProperty("申请单类型")
    private ApplicationTypeEnum applicationType;

    @ApiModelProperty("设备名称")
    private String deviceName;

    @ApiModelProperty("申请开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @ApiModelProperty("申请结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    public ApplicationTypeEnum getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(ApplicationTypeEnum applicationType) {
        this.applicationType = applicationType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
