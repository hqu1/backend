package com.hqu.demon.pojo.request;

import com.hqu.demon.enums.ApplicationStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 申请单基础请求参数
 */
@ApiModel("进口设备申请单")
public class BaseApplicationRequest {

    @ApiModelProperty("申请单位ID")
    private Long applierOrganId;

    @ApiModelProperty("其它信息")
    private String info;

    @ApiModelProperty("申请单状态:DRAFT：草稿（保存），APPROVING：审核中（提交）")
    private ApplicationStatusEnum applicationStatus;

    public Long getApplierOrganId() {
        return applierOrganId;
    }

    public void setApplierOrganId(Long applierOrganId) {
        this.applierOrganId = applierOrganId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ApplicationStatusEnum getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(ApplicationStatusEnum applicationStatus) {
        this.applicationStatus = applicationStatus;
    }
}
