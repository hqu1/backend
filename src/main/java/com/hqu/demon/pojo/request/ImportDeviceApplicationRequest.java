package com.hqu.demon.pojo.request;

import io.swagger.annotations.ApiModel;

@ApiModel("进口设备申请单")
public class ImportDeviceApplicationRequest extends BaseApplicationRequest {
}
