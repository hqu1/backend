package com.hqu.demon.pojo.response;

import java.util.Date;

public class ResourceVO {

    private Long resourceId;

    /**
     * 资源名
     */
    private String name;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件 url
     */
    private String url;

    /**
     * 文件大小
     */
    private long size;

    /**
     * mine 类型
     */
    private String mimeType;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 上传资源的用户 ID
     */
    private Long userId;

    public Long getResourceId() {
        return resourceId;
    }

    public ResourceVO setResourceId(Long resourceId) {
        this.resourceId = resourceId;
        return this;
    }

    public String getName() {
        return name;
    }

    public ResourceVO setName(String name) {
        this.name = name;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public ResourceVO setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public ResourceVO setUrl(String url) {
        this.url = url;
        return this;
    }

    public long getSize() {
        return size;
    }

    public ResourceVO setSize(long size) {
        this.size = size;
        return this;
    }

    public String getMimeType() {
        return mimeType;
    }

    public ResourceVO setMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public ResourceVO setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public ResourceVO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }
}
