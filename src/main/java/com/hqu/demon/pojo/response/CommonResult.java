package com.hqu.demon.pojo.response;

import com.hqu.demon.exception.BusinessException;

import java.io.Serializable;

public class CommonResult<T> implements Serializable {

    public static final int SUCCESS = 0;

    private Boolean success = true;

    private int code = SUCCESS;

    private String msg = "操作成功";

    private T data;

    public CommonResult() {
        super();
    }

    public CommonResult(T data) {
        super();
        this.data = data;
    }

    public CommonResult(BusinessException exception) {
        super();
        this.code = exception.getCode();
        this.msg = exception.getMsg();
        this.success = true;
    }

    public Boolean getSuccess() {
        return success;
    }

    public int getCode() {
        return code;
    }

    public T getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }
}
