package com.hqu.demon.pojo.response;

import com.hqu.demon.entity.Application;
import com.hqu.demon.entity.Approval;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("申请详情对象")
public class BaseApplicationDetailVO {
    @ApiModelProperty("申请单")
    private Application application;
    @ApiModelProperty("审批列表")
    private List<Approval> approvals;

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public List<Approval> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<Approval> approvals) {
        this.approvals = approvals;
    }
}
