package com.hqu.demon.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hqu.demon.business.ApplicationFactory;
import com.hqu.demon.dao.ApplicationMapper;
import com.hqu.demon.entity.Application;
import com.hqu.demon.pojo.request.CheckDeviceApplicationRequest;
import com.hqu.demon.pojo.request.ImportDeviceApplicationRequest;
import com.hqu.demon.pojo.request.QueryApplicationRequest;
import com.hqu.demon.pojo.request.ValuableDeviceApplicationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ApplicationService extends ServiceImpl<ApplicationMapper, Application> {

    @Autowired
    private ApplicationMapper applicationMapper;

    @Autowired
    private ApplicationFactory applicationFactory;

    @Autowired
    private ApprovalService approvalService;

    public Application getAllApplicationById(Long applicationId) {
        return applicationMapper.selectByPrimaryKey(applicationId);
    }

    @Transactional
    public Application createValuableDeviceApplication(ValuableDeviceApplicationRequest request, Long userId) {
        Application application = applicationFactory.createValuableDeviceApplication(request, userId);
        applicationMapper.insert(application);
        approvalService.initApplicationApprovals(application);
        return application;
    }

    @Transactional
    public Application createImportDeviceApplication(ImportDeviceApplicationRequest request, Long userId) {
        Application application = applicationFactory.createImportDeviceApplication(request, userId);
        applicationMapper.insert(application);
        approvalService.initApplicationApprovals(application);
        return application;
    }

    @Transactional
    public Application createCheckDeviceApplication(CheckDeviceApplicationRequest request, Long userId) {
        Application application = applicationFactory.createCheckDeviceApplication(request, userId);
        applicationMapper.insert(application);
        approvalService.initApplicationApprovals(application);
        return application;
    }

    public Application getApplicationDetail(long applicationId) {
        return applicationMapper.getApplicationDetail(applicationId);
    }

    public List<Application> findApplicationList(QueryApplicationRequest request, Long userId) {
        Page<Application> page = new Page<>(request.getCurrentPage(), request.getPageSize());
        return applicationMapper.queryApplicationList(page, request.getApplicationType(), request.getDeviceName(),
                request.getStartDate(), request.getEndDate(), userId).getRecords();
    }
}
