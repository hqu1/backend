package com.hqu.demon.service;

import com.hqu.demon.dao.ResourceMapper;
import com.hqu.demon.entity.Resource;
import com.hqu.demon.service.interfaces.IFileService;
import com.hqu.demon.utils.FileUtils;
import eu.medsea.mimeutil.MimeUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ResourceService {

    @Autowired
    private IFileService fileService;

    @Autowired
    private ResourceMapper resourceMapper;

    /**
     * 上传多个资源文件
     *
     * @param files
     * @param dirPath
     * @param userId
     * @return
     */
    public List<Resource> uploadMultipartFiles(MultipartFile[] files, String dirPath, Long userId) {
        return Stream.of(files).map(file -> uploadMultipartFile(file, dirPath, file.getOriginalFilename(), userId)).collect(Collectors.toList());
    }

    /**
     * 上传资源文件
     *
     * @param file
     * @param dirPath
     * @param name
     * @param userId
     * @return
     */
    public Resource uploadMultipartFile(MultipartFile file, String dirPath, String name, Long userId) {
        if (file.isEmpty()) {
            return null;
        }
        // 验证文件 MD5 如果存在则返回已存在的资源对象
        String fingerPrint = "";
        String mineType = "";
        try {
            byte[] fileData = file.getBytes();
            fingerPrint = DigestUtils.md5Hex(fileData);
            Resource existResource = findResourceByFingerPrint(fingerPrint);
            if (existResource != null) {
                return existResource;
            }
            MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
            mineType = MimeUtil.getMimeTypes(file.getBytes()).toString();
            mineType = FileUtils.getFileMineType(fileData);
        } catch (IOException e) {
            e.printStackTrace();
            if (StringUtils.isEmpty(fingerPrint)) {
                fingerPrint = UUID.randomUUID().toString();
            }
        }
        String fileName = fingerPrint + FileUtils.getExtensionName(name);
        if (fileService.handleUploadMultipartFile(file, dirPath, fileName)) {
            // 保存资源
            Resource resource = new Resource();
            resource.setPath(dirPath);
            resource.setName(name);
            resource.setFileName(fingerPrint + FileUtils.getExtensionName(name));
            resource.setFingerPrint(fingerPrint);
            resource.setMimeType(mineType);
            resource.setSize(file.getSize());
            resource.setUserId(userId);
            resourceMapper.insert(resource);
            return resourceMapper.selectById(resource.getResourceId());
        }
        return null;
    }

    public Resource findResourceByFingerPrint(String fingerPrint) {
        if (!StringUtils.isEmpty(fingerPrint)) {
            // 查询
            return resourceMapper.findByFingerPrint(fingerPrint);
        }
        return null;
    }

    public Resource getResourceById(Long resourceId) {
        if (resourceId != null && resourceId > 0) {
            // 查询
            return resourceMapper.selectById(resourceId);
        }
        return null;
    }

}
