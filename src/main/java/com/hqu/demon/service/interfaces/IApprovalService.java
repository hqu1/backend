package com.hqu.demon.service.interfaces;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hqu.demon.entity.Approval;
import org.springframework.stereotype.Service;

@Service
public  interface IApprovalService extends IService<Approval> {

}