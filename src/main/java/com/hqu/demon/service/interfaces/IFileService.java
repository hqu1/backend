package com.hqu.demon.service.interfaces;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务:当文件上传时进行相应的保存
 */
@Service
public interface IFileService {

    /**
     * 处理文件
     *
     * @param file
     * @param dirPath
     * @param fileName
     * @return
     */
    public Boolean handleUploadMultipartFile(MultipartFile file, String dirPath, String fileName);
}
