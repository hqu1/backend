package com.hqu.demon.service;

import com.hqu.demon.service.interfaces.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
@Primary
public class InternalServerFileService implements IFileService {

    private static final Logger logger = LoggerFactory.getLogger(InternalServerFileService.class);

    @Override
    public Boolean handleUploadMultipartFile(MultipartFile file, String dirPath, String fileName) {
        if (!file.isEmpty()) {
            String fullFilePath = dirPath + fileName;
            File desFile = new File(fullFilePath);
            try {
                file.transferTo(desFile);
            } catch (IOException e) {
                e.printStackTrace();
                return Boolean.FALSE;
            }
            logger.info("文件上传成功 文件地址=>" + fullFilePath);
        }
        return Boolean.TRUE;
    }
}
