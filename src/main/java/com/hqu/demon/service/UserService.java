package com.hqu.demon.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hqu.demon.dao.UserMapper;
import com.hqu.demon.entity.User;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService extends ServiceImpl<UserMapper, User> {

    public void saveUserToken(Long userId, String token) {

    }

    public User findByUsername(String username) {
        return null;
    }

    public String createToken(String username) {
        User queryUser = new User();
        queryUser.setUsername(username);
        QueryWrapper queryWrapper = new QueryWrapper(queryUser);
        User user = getOne(queryWrapper);
        if (user != null) {
            String token = UUID.randomUUID().toString();
            user.setToken(token);
            updateById(user);
            return token;
        } else {
            return null;
        }
    }
}
