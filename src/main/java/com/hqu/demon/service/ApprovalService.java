package com.hqu.demon.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hqu.demon.business.ApprovalFactory;
import com.hqu.demon.business.utils.EnumUtils;
import com.hqu.demon.dao.ApprovalMapper;
import com.hqu.demon.entity.Application;
import com.hqu.demon.entity.Approval;
import com.hqu.demon.enums.ApplicationStatusEnum;
import com.hqu.demon.enums.ApprovalStatusEnum;
import com.hqu.demon.enums.ErrorCodeEnum;
import com.hqu.demon.exception.BusinessException;
import com.hqu.demon.exception.CannotFindEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ApprovalService extends ServiceImpl<ApprovalMapper, Approval> {

    @Autowired
    private ApprovalMapper approvalMapper;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private ApprovalFactory approvalFactory;

    /**
     * 初始化申请审批
     *
     * @param application
     * @return
     */
    public List<Approval> initApplicationApprovals(Application application) {
        List<Approval> approvals = approvalFactory.createApplicationApprovals(application);
        this.saveBatch(approvals);
        return approvals;
    }

    @Transactional
    public boolean approve(long approvalId, long userId, ApprovalStatusEnum status, String advice) {
        Approval approval = approvalMapper.selectById(approvalId);
        if (approval == null) {
            throw new CannotFindEntityException(ErrorCodeEnum.APPROVAL_NOT_EXIST);
        }
        Application application = applicationService.getApplicationDetail(approval.getApplicationId());
        if (application == null) {
            throw new CannotFindEntityException(ErrorCodeEnum.APPLICATION_NOT_EXIST);
        }
        if (!application.canBeApproved()) {
            throw new BusinessException(ErrorCodeEnum.APPLICATION_INVALID_STATUS);
        }
        if (!approval.canBeApproved()) {
            throw new BusinessException(ErrorCodeEnum.APPLICATION_INVALID_STATUS);
        }
//        approval.setStatus(status);
        approval.setAdvice(advice);
        updateById(approval);
        if (status == ApprovalStatusEnum.APPROVED && application.getApprovalList().size() > approval.getSequence() + 1) {
            // 审批通过并还需要其它人审批，则继续审批
            application.getApprovalList().get(approval.getSequence() + 1).setStatus(ApprovalStatusEnum.WAITING);
            updateById(application.getApprovalList().get(approval.getSequence() + 1));
        } else {
            // 最后一个审批已通过，或者审批被拒绝时，更新申请审批状态
            application.setStatus(EnumUtils.convertFromApprovalStatus(status));
            applicationService.updateById(application);
        }
        return true;
    }

}
